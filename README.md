## Getting Started

To start the application, follow these steps:

1. Build the Docker containers:

   docker-compose build

2. Start the Docker containers:

    docker-compose up

3. Open your web browser and go to http://localhost:10107/api/doc to access the API documentation.


## Run tests

1. composer require --dev phpunit/phpunit
2. php bin/console doctrine:database:create --env=test
3. php bin/console doctrine:migrations:migrate --env=test
4. vendor/bin/phpunit

# Blog Application

This simple blog application is designed with modern software development principles, including Symfony, DDD (Domain-Driven Design), Hexagonal Architecture, CQRS (Command Query Responsibility Segregation), and Swagger OpenAPI.

Application Specification
The goal of this task is to create a basic blog application that allows users to:

### Add new posts using:

API
- Command Line Interface (CLI)
- Web form
- List posts through a paginated API, providing information about the total number of added posts.

Retrieve a single post by its identifier.

Additionally, upon successful post creation, the author should receive a notification (e.g., email). Authentication is not required for this application.

### Technologies Used
- **Symfony**: A robust PHP web application framework.
- **DDD (Domain-Driven Design)**: A set of principles and patterns for designing and implementing complex software.
- **Hexagonal Architecture**: A software architecture that focuses on separating the application core from its external concerns.
- **CQRS (Command Query Responsibility Segregation)**: A pattern that segregates the operations that read data (queries) from the operations that update data (commands).
- **Swagger OpenAPI**: A toolset that helps design, build, document, and consume RESTful APIs.

### Usage

#### Adding a New Post
Users can add new posts through the following methods:

API: Utilizing the provided API endpoints.

``http://localhost:10107/api/doc``

CLI: Using command-line commands.

``curl -X POST -H "Content-Type: application/json" -d '{"title": "Example Title", "content": "Lorem ipsum dolor sit amet, consectetur adipiscing elit.", "fileId": 1}' http://localhost/api/post``

Web Form: Submitting posts through a web-based form.

#### Listing Posts
The application supports listing posts through a paginated API, providing details about the total number of posts.

``http://localhost:10107/api/posts?page=1&limit=5``

### Retrieving a Single Post
A dedicated endpoint allows users to retrieve a specific post by providing its identifier.

``http://localhost:10107/api/post/2``

### Notifications
Upon successful post creation, the author will receive a notification, for example, an email.