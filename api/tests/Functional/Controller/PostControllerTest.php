<?php

declare(strict_types=1);

namespace App\Tests\Functional\Controller;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;

class PostControllerTest extends WebTestCase
{
    private KernelBrowser $client;

    public function testPosts(): void
    {
        $this->client = self::createClient();

        $fileId = $this->addFile();
        $this->addPost($fileId);
        $this->addPost($fileId);
        $this->getPosts();
    }

    private function addFile(): int
    {
        $fileContent = 'Hello, this is a test file content.';
        $filePath = __DIR__ . '/test.jpg';
        file_put_contents($filePath, $fileContent);

        $uploadedFile = new UploadedFile(
            __DIR__ . '/test.jpg',
            'test.jpg'
        );

        $this->client->request(
            'POST',
            '/api/file',
            [],
            ['file' => $uploadedFile],
            ['CONTENT_TYPE' => 'multipart/form-data']
        );

        $this->assertEquals(Response::HTTP_OK,  $this->client->getResponse()->getStatusCode());

        $content =  $this->client->getResponse()->getContent();
        $data = json_decode($content, true);

        return $data['fileId'];
    }

    private function addPost(int $fileId)
    {
        $data['title'] = "Lorem ipsum";
        $data['content'] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.";
        $data['fileId'] = $fileId;

        $this->client ->request(
            'POST',
            '/api/post',
            [],
            [],
            ['CONTENT_TYPE' => 'application/json'],
            json_encode($data)
        );

        $this->assertEquals(Response::HTTP_OK, $this->client ->getResponse()->getStatusCode());
    }

    private function getPosts()
    {
        $this->client ->request('GET', '/api/posts');

        $this->assertEquals(Response::HTTP_OK, $this->client ->getResponse()->getStatusCode());

        $content = $this->client ->getResponse()->getContent();
        $data = json_decode($content, true);

        $this->assertArrayHasKey('page', $data);
        $this->assertArrayHasKey('limit', $data);
        $this->assertArrayHasKey('total', $data);
        $this->assertArrayHasKey('posts', $data);
    }

}


