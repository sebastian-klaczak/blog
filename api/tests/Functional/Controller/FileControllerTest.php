<?php

declare(strict_types=1);

namespace App\Tests\Functional\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;

class FileControllerTest extends WebTestCase
{
    public function testAddFile(): void
    {
        $client = static::createClient();

        $fileContent = 'Hello, this is a test file content.';
        $filePath = __DIR__ . '/test.jpg';
        file_put_contents($filePath, $fileContent);

        $uploadedFile = new UploadedFile(
            __DIR__ . '/test.jpg',
            'test.jpg'
        );

        $client->request(
            'POST',
            '/api/file',
            [],
            ['file' => $uploadedFile],
            ['CONTENT_TYPE' => 'multipart/form-data']
        );

        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());

        $content = $client->getResponse()->getContent();
        $data = json_decode($content, true);

        $this->assertArrayHasKey('fileId', $data);
        $this->assertArrayHasKey('fileName', $data);
    }

}


