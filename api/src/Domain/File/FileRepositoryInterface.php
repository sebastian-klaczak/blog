<?php

declare(strict_types=1);

namespace App\Domain\File;

use App\Domain\Post\Post;
use App\Domain\Post\Validation\ValidationRule\ContentRule;
use App\Domain\Post\Validation\ValidationRule\TitleRule;

interface FileRepositoryInterface {

    public function save(string $fileName);

}
