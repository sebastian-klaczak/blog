<?php

declare(strict_types=1);

namespace App\Domain\File\Validation\ValidationRule;

use App\Domain\File\Validation\ValidationException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class SizeRule
{
    public function check(
        UploadedFile $uploadedFile
    ): bool {

        $maxFileSize = 1 * 1024 * 1024; // 1 MB in bytes

        $size = $uploadedFile->getSize();

        if ($size <= $maxFileSize) {
            return true;
        } else {
            throw new ValidationException('File size must be at most 1 MB.');
        }

    }
}
