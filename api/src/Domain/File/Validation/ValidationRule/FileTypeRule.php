<?php

declare(strict_types=1);

namespace App\Domain\File\Validation\ValidationRule;

use App\Domain\File\Validation\ValidationException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileTypeRule
{
    public function check(UploadedFile $uploadedFile): bool
    {
        $allowedExtensions = ['jpg'];

        $extension = $uploadedFile->getClientOriginalExtension();

        if (in_array($extension, $allowedExtensions, true)) {
            return true;
        } else {
            throw new ValidationException('File must be in JPG format.');
        }
    }
}
