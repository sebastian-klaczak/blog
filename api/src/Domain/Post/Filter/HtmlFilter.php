<?php

declare(strict_types=1);

namespace App\Domain\Post\Filter;

class HtmlFilter
{
    // Define allowed HTML tags
    private array $allowedTags = ['ul', 'li', 'ol', 'p', 'strong'];

    /**
     * Filter the provided data by sanitizing and stripping HTML tags.
     * @param array $data The data to be filtered.
     * @return array The filtered data.
     */
    public function filter(string $key, string $value): string
    {
        if ($key === 'content') {
            return $this->sanitizeHtml($value);
        } else {
            // Validate and strip HTML tags from other fields
            return $this->stripHtmlTags($value);
        }
    }

    /**
     * Sanitize HTML content by allowing only specified tags.
     * @param string $input The HTML input to be sanitized.
     * @return string The sanitized HTML.
     */
    private function sanitizeHtml(string $input): string
    {
        // Use HTMLPurifier library to allow only specified tags
        $config = [
            'HTML.Allowed' => implode(',', $this->allowedTags),
        ];

        $purifier = new \HTMLPurifier($config);

        return $purifier->purify($input);
    }

    /**
     * Strip all HTML tags from the input.
     * @param string $input The HTML input to be stripped.
     * @return string The input with HTML tags stripped.
     */
    private function stripHtmlTags(string $input): string
    {
        // Strip all HTML tags
        return strip_tags($input);
    }
}