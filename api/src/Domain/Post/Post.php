<?php

declare(strict_types=1);

namespace App\Domain\Post;

use App\Domain\Post\Filter\HtmlFilter;
use App\Domain\Post\Validation\ValidationRule\ContentRule;
use App\Domain\Post\Validation\ValidationRule\TitleRule;

class Post
{
    private ?int $id = null;
    private string $title;
    private string $content;
    private int $fileId;

    public function __construct(
        string $title,
        string $content,
        int $fileId
    )
    {
        $htmlFilter = new HtmlFilter();
        $this->title = $htmlFilter->filter('title', $title);
        $this->content = $htmlFilter->filter('content', $content);
        $this->fileId = $fileId;

        $this->isValid();
    }

    private function isValid(): bool
    {
        (new TitleRule())->check($this);
        (new ContentRule())->check($this);

        return true;
    }

    public function setId(int $id): void
    {
        if ($this->id === null) {
            $this->id = $id;
        }
    }
    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function getFileId(): int
    {
        return $this->fileId;
    }

}
