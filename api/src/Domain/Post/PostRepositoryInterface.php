<?php

declare(strict_types=1);

namespace App\Domain\Post;

use App\Domain\Post\Validation\ValidationRule\ContentRule;
use App\Domain\Post\Validation\ValidationRule\TitleRule;

interface PostRepositoryInterface {

    public function save(Post $post): void;

}
