<?php

declare(strict_types=1);

namespace App\Domain\Post\Validation\ValidationRule;

use App\Domain\Post\Post;
use App\Domain\Post\Validation\ValidationException;

class ContentRule
{
    public function check(
        Post $post
    ): bool {

        $title = $post->getContent();

        if ($title && mb_strlen($title) >= 20) {
            return true;
        } else {
            throw new ValidationException('Content must be at least 20 characters long.');
        }
        
    }
}