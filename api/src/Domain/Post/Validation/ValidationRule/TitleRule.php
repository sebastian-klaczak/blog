<?php

declare(strict_types=1);

namespace App\Domain\Post\Validation\ValidationRule;

use App\Domain\Post\Post;
use App\Domain\Post\Validation\ValidationException;

class TitleRule
{
    public function check(
        Post $post
    ): bool {

        $title = $post->getTitle();

        if ($title && mb_strlen($title) >= 10 && mb_strlen($title) <= 80) {
            return true;
        } else {
            throw new ValidationException('Title must be between 10 and 80 characters long.');
        }

    }
}
