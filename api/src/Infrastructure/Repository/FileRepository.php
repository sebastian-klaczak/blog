<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Infrastructure\Entity\File;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class FileRepository extends ServiceEntityRepository
{
    private PostRepository $employeeRepository;

    public function __construct(
        ManagerRegistry $registry,
    ){
        parent::__construct($registry, File::class);
    }

    public function save(string $fileName): int
    {
        $fileEntity = new File();
        $fileEntity->setPath($fileName);
        $this->_em->persist($fileEntity);
        $this->_em->flush();

        return $fileEntity->getId();
    }

}
