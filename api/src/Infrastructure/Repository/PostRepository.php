<?php

declare(strict_types=1);

namespace App\Infrastructure\Repository;

use App\Domain\Post\PostRepositoryInterface;
use App\Domain\Post\Validation\ValidationException;
use App\Infrastructure\Entity\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;

class PostRepository extends ServiceEntityRepository implements PostRepositoryInterface
{
    private FileRepository $fileRepository;
    public function __construct (
        ManagerRegistry $registry,
        FileRepository $fileRepository
    ) {
        $this->fileRepository = $fileRepository;
        parent::__construct($registry, Post::class);
    }

    public function save(\App\Domain\Post\Post $post): void
    {
        $postEntity = new Post();
        $postEntity->setTitle($post->getTitle());
        $postEntity->setContent($post->getContent());

        $file = $this->fileRepository->find($post->getFileId());

        if ($file) {
            $postEntity->setImage($file);
        } else {
            throw new ValidationException('File not exist.');
        }

        $this->_em->persist($postEntity);
        $this->_em->flush();

        $post->setId($postEntity->getId());
    }

    public function countAll(): int
    {
        return $this->createQueryBuilder('p')
            ->select('COUNT(p.id)')
            ->getQuery()
            ->getSingleScalarResult();
    }
    public function findAllPaginated(?int $page, ?int $limit): Paginator
    {
        $query = $this->createQueryBuilder('p')->getQuery();

        $paginator = new \Doctrine\ORM\Tools\Pagination\Paginator($query);
        $paginator->getQuery()
            ->setFirstResult($limit * ($page - 1))
            ->setMaxResults($limit);

        return $paginator;
    }

}
