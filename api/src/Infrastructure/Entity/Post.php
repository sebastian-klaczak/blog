<?php

declare(strict_types=1);

namespace App\Infrastructure\Entity;

use App\Infrastructure\Repository\PostRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'post', schema: 'public')]
#[ORM\Entity(repositoryClass: PostRepository::class)]
class Post
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: 'post_id', type: 'integer')]
    private int $id;

    #[ORM\Column(name: 'title', type: 'string', length: 80)]
    private string $title;

    #[ORM\Column(name: 'content', type: 'text')]
    private string $content;

    #[ORM\ManyToOne(targetEntity: File::class)]
    #[ORM\JoinColumn(name: 'file_id', referencedColumnName: 'file_id', nullable: false)]
    private File $image;

    public function getId(): int
    {
        return $this->id;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        $this->title = $title;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
    }

    public function getImage(): File
    {
        return $this->image;
    }

    public function setImage(File $image): void
    {
        $this->image = $image;
    }

}
