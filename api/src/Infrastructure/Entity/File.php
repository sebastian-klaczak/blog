<?php

declare(strict_types=1);

namespace App\Infrastructure\Entity;

use App\Infrastructure\Repository\FileRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Table(name: 'file', schema: 'public')]
#[ORM\Entity(repositoryClass: FileRepository::class)]
class File
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(name: 'file_id', type: 'integer')]
    private int $id;

    #[ORM\Column(name: 'path', type: 'string')]
    private string $path;

    public function getId(): int
    {
        return $this->id;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function setPath(string $path): void
    {
        $this->path = $path;
    }

}


