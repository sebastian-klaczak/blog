<?php

namespace App;

use Symfony\Bundle\FrameworkBundle\Kernel\MicroKernelTrait;
use Symfony\Component\HttpKernel\Kernel as BaseKernel;

set_time_limit(0);

class Kernel extends BaseKernel
{
    use MicroKernelTrait;
}
