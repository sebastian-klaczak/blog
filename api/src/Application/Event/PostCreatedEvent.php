<?php

declare(strict_types=1);

namespace App\Application\Event;

use Symfony\Contracts\EventDispatcher\Event;

class PostCreatedEvent extends Event
{
    private int $postId;

    public function __construct(int $postId)
    {
        $this->postId = $postId;
    }

    public function getPostId(): int
    {
        return $this->postId;
    }
}