<?php

declare(strict_types=1);

namespace App\Application\Controller;

use App\Application\Model\AddPostCommand;
use App\Application\Model\GetPostQuery;
use App\Application\Model\GetPostsQuery;
use App\Application\Model\GetPostsResponse;
use App\Domain\Post\Validation\ValidationException;
use App\Domain\Post\Post;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Method contains operations related to post
 *
 * @OA\Tag(name="Post")
 */
class PostController extends AbstractController
{
    use HandleTrait;

    private MessageBusInterface $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    /**
     * @OA\Post(
     *     summary="Add new post",
     *     @OA\RequestBody(
     *         description="Post data",
     *         @OA\JsonContent(
     *             ref=@Model(type=AddPostCommand::class)
     *         )
     *     )
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="Ok",
     *     @OA\JsonContent(
     *         type="null"
     *     )
     * )
     *
     * @OA\Response(
     *     response=400,
     *     description="Validation error",
     *     @OA\JsonContent(
     *         @OA\Property(
     *             property="error",
     *             type="string",
     *             example="Title must be between 10 and 80 characters long."
     *         )
     *     )
     * )
     *
     */
    #[Route('api/post', name: 'post_add', methods: ['POST'])]
    public function add(
        Request $request,
        SerializerInterface $serializer
    ): JsonResponse {

        /** @var AddPostCommand $addPostCommand */
        $addPostCommand = $serializer->deserialize(
            $request->getContent(),
            AddPostCommand::class,
            'json'
        );

        try {

            $this->messageBus->dispatch($addPostCommand);

        } catch (ValidationException $e) {

            return new JsonResponse(
                $serializer->serialize($e->getMessage(), 'json'),
                Response::HTTP_BAD_REQUEST,
                [],
                true
            );

        }

        return new JsonResponse(
            $serializer->serialize(null, 'json'),
            Response::HTTP_OK,
            [],
            true
        );


    }

    /**
     * @OA\Get(
     *     summary="Get post",
     *     ref=@Model(type=GetPostQuery::class)
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="Ok",
     *     @OA\JsonContent(
     *        ref=@Model(type=Post::class)
     *     )
     * )
     *
     *  @OA\Response(
     *     response=404,
     *     description="Not Found",
     *     @OA\JsonContent(
     *         type="null"
     *     )
     * )
     *
     */
    #[Route('api/post/{id}', name: 'post_get', methods: ['GET'])]
    public function get(
        SerializerInterface $serializer,
        int $id
    ): JsonResponse {

        $getPostQuery = new GetPostQuery($id);

        $post = $this->handle($getPostQuery);

        if ($post) {

            return new JsonResponse(
                $serializer->serialize($post, 'json'),
                Response::HTTP_OK,
                [],
                true
            );

        } else {
            return new JsonResponse(
                $serializer->serialize(null, 'json'),
                Response::HTTP_NOT_FOUND,
                [],
                true
            );
        }
    }

    /**
     * @OA\Get(
     *     summary="Get all posts paginated",
     *     ref=@Model(type=GetPostsQuery::class)
     * )
     *
     * @OA\Response(
     *     response=200,
     *     description="Ok",
     *     @OA\JsonContent(
     *        ref=@Model(type=GetPostsResponse::class)
     *     )
     * )
     *
     */
    #[Route('api/posts', name: 'post_list', methods: ['GET'])]
    public function list(
        Request $request,
        SerializerInterface $serializer,
    ): JsonResponse {

        $page = $request->query->getInt('page', 1);
        $limit = $request->query->getInt('limit', 10);

        $getPostsQuery = new GetPostsQuery($page, $limit);

        $getPostsResponse = $this->handle($getPostsQuery);

        return new JsonResponse(
            $serializer->serialize($getPostsResponse, 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }

}
