<?php

declare(strict_types=1);

namespace App\Application\Controller;

use App\Application\Model\AddFileCommand;
use App\Domain\File\Validation\ValidationException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * Method contains operations related to file
 *
 * @OA\Tag(name="File")
 */
class FileController extends AbstractController
{
    use HandleTrait;

    private MessageBusInterface $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    /**
     * @OA\Post(
     *     summary="Add new file",
     *     @OA\RequestBody(
     *         required=true,
     *         description="File data",
     *         @OA\MediaType(
     *             mediaType="multipart/form-data",
     *             @OA\Schema(
     *                 @OA\Property(
     *                     property="file",
     *                     description="The file to upload",
     *                     type="string",
     *                     format="binary",
     *                 ),
     *             ),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=200,
     *         description="Ok",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="file_name", type="string", example="unique_file_name.jpg"),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=400,
     *         description="Bad Request",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="error", type="string", example="No file uploaded."),
     *         ),
     *     ),
     *     @OA\Response(
     *         response=500,
     *         description="Internal Server Error",
     *         @OA\JsonContent(
     *             type="object",
     *             @OA\Property(property="error", type="string", example="Unable to upload the file."),
     *         ),
     *     ),
     * )
     */
    #[Route('api/file', name: 'file_add', methods: ['POST'])]
    public function add(
        Request             $request,
        SerializerInterface $serializer
    ): JsonResponse {

        $addFileCommand = new AddFileCommand(
            $request->files->get('file'),
            $this->getParameter('kernel.project_dir')
        );

        try {
            $addFileResponse = $this->handle($addFileCommand);
        } catch (ValidationException $e) {
            return new JsonResponse(
                $serializer->serialize($e->getMessage(), 'json'),
                Response::HTTP_BAD_REQUEST,
                [],
                true
            );
        }

        return new JsonResponse(
            $serializer->serialize($addFileResponse, 'json'),
            Response::HTTP_OK,
            [],
            true
        );

    }
}
