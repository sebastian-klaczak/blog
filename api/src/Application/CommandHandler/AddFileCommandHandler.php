<?php

declare(strict_types=1);

namespace App\Application\CommandHandler;

use App\Application\Model\AddFileCommand;
use App\Application\Model\AddFileResponse;
use App\Domain\File\Validation\ValidationException;
use App\Domain\File\Validation\ValidationRule\FileTypeRule;
use App\Domain\File\Validation\ValidationRule\SizeRule;
use App\Infrastructure\Repository\FileRepository;

class AddFileCommandHandler
{
    private FileRepository $fileRepository;

    public function __construct(
        FileRepository $fileRepository,
    ) {
        $this->fileRepository = $fileRepository;
    }

    public function __invoke(AddFileCommand $addFileCommand): AddFileResponse
    {
        $uploadedFile = $addFileCommand->uploadedFile;

        if (!$uploadedFile) {
            throw new ValidationException('No file uploaded.');
        }

        (new FileTypeRule())->check($uploadedFile);
        (new SizeRule())->check($uploadedFile);

        $fileName = md5(uniqid()) . '.' . $uploadedFile->guessExtension();
        $uploadedFile->move($addFileCommand->projectDir . '/public/img', $fileName);

        $fileId = $this->fileRepository->save($fileName);

        return new AddFileResponse(
            $fileId,
            $fileName
        );
    }
}