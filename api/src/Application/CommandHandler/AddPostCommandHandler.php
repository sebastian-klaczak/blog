<?php

declare(strict_types=1);

namespace App\Application\CommandHandler;

use App\Application\Event\PostCreatedEvent;
use App\Application\Model\AddPostCommand;
use App\Domain\Post\Post;
use App\Domain\Post\PostRepositoryInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

class AddPostCommandHandler
{

    private PostRepositoryInterface $postRepository;
    private EventDispatcherInterface $eventDispatcher;

    public function __construct(
        PostRepositoryInterface $postRepository,
        EventDispatcherInterface $eventDispatcher,
    ) {
        $this->postRepository = $postRepository;
        $this->eventDispatcher = $eventDispatcher;

    }

    public function __invoke(AddPostCommand $addPostCommand): void
    {
        $post = new Post(
            $addPostCommand->title,
            $addPostCommand->content,
            $addPostCommand->fileId
        );

        $this->postRepository->save($post);

        $this->eventDispatcher->dispatch(new PostCreatedEvent($post->getId()), PostCreatedEvent::class);
    }
}