<?php

declare(strict_types=1);

namespace App\Application\QueryHandler;

use App\Application\Model\GetPostsQuery;
use App\Application\Model\GetPostsResponse;
use App\Domain\Post\Post;
use App\Domain\Post\PostRepositoryInterface;

class GetPostsQueryHandler
{

    private PostRepositoryInterface $postRepository;

    public function __construct(
        PostRepositoryInterface $postRepository,
    ) {
        $this->postRepository = $postRepository;
    }

    public function __invoke(GetPostsQuery $getPostQuery): GetPostsResponse
    {
        /** @var \App\Infrastructure\Entity\Post $postEntity */
        $postEntities = $this->postRepository->findAllPaginated($getPostQuery->page, $getPostQuery->limit);

        $posts = [];

        foreach ($postEntities->getIterator()->getArrayCopy() as $postEntity) {
            $post = new Post(
                $postEntity->getTitle(),
                $postEntity->getContent(),
                $postEntity->getImage()->getId()
            );
            $post->setId($postEntity->getId());

            $posts[] = $post;
        }

        return new GetPostsResponse(
            $getPostQuery->page,
            $getPostQuery->limit,
            $this->postRepository->countAll(),
            $posts
        );

    }
}