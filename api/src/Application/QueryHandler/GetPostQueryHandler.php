<?php

declare(strict_types=1);

namespace App\Application\QueryHandler;

use App\Application\Model\GetPostQuery;
use App\Domain\Post\Post;
use App\Domain\Post\PostRepositoryInterface;

class GetPostQueryHandler
{

    private PostRepositoryInterface $postRepository;

    public function __construct(
        PostRepositoryInterface $postRepository,
    ) {
        $this->postRepository = $postRepository;

    }

    public function __invoke(GetPostQuery $getPostQuery): ?Post
    {
        /** @var \App\Infrastructure\Entity\Post $postEntity */
        $postEntity = $this->postRepository->find($getPostQuery->postId);

        if (!$postEntity) {
            return null;
        }

        $post = new Post(
            $postEntity->getTitle(),
            $postEntity->getContent(),
            $postEntity->getImage()->getId()
        );

        $post->setId($postEntity->getId());

        return $post;
    }
}