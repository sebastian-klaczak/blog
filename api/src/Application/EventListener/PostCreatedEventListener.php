<?php

declare(strict_types=1);

namespace App\Application\EventListener;

use App\Application\Event\PostCreatedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class PostCreatedEventListener implements EventSubscriberInterface
{
    private MailerInterface $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    public static function getSubscribedEvents(): array
    {
        return [
            PostCreatedEvent::class => 'onPostCreated',
        ];
    }

    public function onPostCreated(PostCreatedEvent $event): void
    {
        $email = (new Email())
            ->from('blix@example.com')
            ->to('s.klaczak@gmail.com')
            ->subject('Nowy post utworzony')
            ->text('Nowy post został utworzony o numerze: ' . $event->getPostId());

        //$this->mailer->send($email);
    }
}