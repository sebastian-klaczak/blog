<?php

declare(strict_types=1);

namespace App\Application\Model;

use OpenApi\Annotations as OA;


/**
 * @OA\Schema(
 *     description="GetPostQuery model",
 *     title="GetPostQuery",
 *     required={"postId"}
 * )
 */
class GetPostQuery
{

    /**
     * @OA\Property(
     *     in="path",
     *     type="integer",
     *     required=true,
     *     description="id of post",
     *     example="123"
     * )
     */
    public int $postId;
    public function __construct(int $postId)
    {
        $this->postId = $postId;
    }

}
