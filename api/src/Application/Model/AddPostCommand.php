<?php

declare(strict_types=1);

namespace App\Application\Model;

use OpenApi\Annotations as OA;

/**
 * @OA\Schema(
 *     description="AddPostCommand model",
 *     title="AddPostCommand",
 *     required={"title", "content", "fileId"}
 * )
 */
class AddPostCommand
{
    /**
     * @OA\Property(
     *     type="string",
     *     description="Title",
     *     example="Lorem ipsum"
     * )
     */
    public string $title;

    /**
     * @OA\Property(
     *     type="string",
     *     description="Content",
     *     example="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua."
     * )
     */
    public string $content;

    /**
     * @OA\Parameter(
     *     type="integer",
     *     description="Id of file",
     *     description="123"
     * )
     */
    public int $fileId;

}
