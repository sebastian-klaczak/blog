<?php

declare(strict_types=1);

namespace App\Application\Model;

use OpenApi\Annotations as OA;
use Symfony\Component\Messenger\MessageBusInterface;

/**
 * @OA\Schema(
 *     description="GetPostsQuery model",
 *     title="GetPostsQuery",
 * )
 */
class GetPostsQuery
{

    /**
     *  @OA\Parameter(
     *      name="page",
     *      in="query",
     *      description="Page number",
     *      required=false,
     *      @OA\Schema(type="integer", example=1)
     *  ),
     */
    public ?int $page = null;

    /**
     *  @OA\Parameter(
     *      name="page",
     *      in="query",
     *      description="Page number",
     *      required=false,
     *      @OA\Schema(type="integer", example=5)
     *  ),
     */
    public ?int $limit = null;
    public function __construct(?int $page, ?int $limit)
    {
        $this->page = $page;
        $this->limit = $limit;
    }

}
