<?php

declare(strict_types=1);

namespace App\Application\Model;

use App\Domain\Post\Post;
use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * @OA\Schema(
 *     description="GetPostsResponse model",
 *     title="GetPostsResponse",
 * )
 */
class GetPostsResponse
{

    /**
     * @OA\Property(
     *     type="integer",
     *     description="Page number",
     *     example="1",
     * )
     */
    public ?int $page = null;

    /**
     * @OA\Property(
     *     type="integer",
     *     description="Number of items per page",
     *     example="10",
     * )
     */
    public ?int $limit = null;

    /**
     * @OA\Property(
     *     type="integer",
     *     description="Total number of posts",
     *     example="30",
     * )
     */
    public int $total = 0;

    /**
     * @OA\Property(
     *     type="array",
     *     @OA\Items(ref=@Model(type=Post::class))
     * )
     * @var Post[]
     */
    public array $posts = [];

    public function __construct(?int $page, ?int $limit, int $total, array $posts)
    {
        $this->page = $page;
        $this->limit = $limit;
        $this->total = $total;
        $this->posts = $posts;
    }

}
