<?php

declare(strict_types=1);

namespace App\Application\Model;

use OpenApi\Annotations as OA;

class AddFileCommand
{
    public $uploadedFile;

    public string $projectDir;

    public function __construct(
        $uploadedFile,
        string $projectDir
    ) {
        $this->uploadedFile = $uploadedFile;
        $this->projectDir = $projectDir;
    }
}
