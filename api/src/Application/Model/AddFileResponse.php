<?php

declare(strict_types=1);

namespace App\Application\Model;

use OpenApi\Annotations as OA;
use Nelmio\ApiDocBundle\Annotation\Model;

/**
 * @OA\Schema(
 *     description="AddFileResponse model",
 *     title="AddFileResponse",
 * )
 */
class AddFileResponse
{

    /**
     * @OA\Property(
     *     type="integer",
     *     description="Id of fole",
     *     example="1",
     * )
     */
    public int $fileId;

    /**
     * @OA\Property(
     *     type="string",
     *     description="File name",
     *     example="d6155abb65153e03f4f41a8c2c036bc2.jpg",
     * )
     */
    public string $fileName;

    public function __construct(int $fileId, string $fileName)
    {
        $this->fileId = $fileId;
        $this->fileName = $fileName;
    }

}
